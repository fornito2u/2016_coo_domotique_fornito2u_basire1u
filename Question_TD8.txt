1.2)

Pour manipuler la classe Lumiere comme un Apprareil, une premi�re solution serait de cr�er une autre classe (LumiereCommandable) 
qui h�rite de la classe Lumiere et qui impl�mente l'interface Commandable. Cette classe contiendra un constructeur vide et 2 m�thodes : 
	Une m�thode allumer qui utilisera changerIntinsite
	Une m�thode �teindre qui utilisera changeIntensite pour mettre l'intensite du variateur � 0.

Une autre solution, similaire mais plus universelle serait de remplacer le lien d'h�ritage entre LumiereCommandable et Lumiere par une association � cardinalit� 0..*
