package Class;

import thermos.Thermostat;

public class mainThermostat {
	public static void main(String args[])
	{
		Telecommande t=new Telecommande();
		
		Thermostat therm1 = new Thermostat();
		ThermostatCommandable adapT1 = new ThermostatCommandable(therm1);
		
		t.ajouterObjet(adapT1);
		
		TelecommandeGraphique tg=new TelecommandeGraphique(t);
	}
}
