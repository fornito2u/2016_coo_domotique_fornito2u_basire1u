package Class;

import thermos.Thermostat;

public class ThermostatCommandable implements Commandable 
{
	private Thermostat Ter;
	
	public ThermostatCommandable(Thermostat termos)
	{
		this.Ter=termos;
	}
	
	public void activer()
	{
		this.Ter.monterTemperature();
	}
	
	public void desactiver()
	{
		this.Ter.baisserTemperature();
	}
}
