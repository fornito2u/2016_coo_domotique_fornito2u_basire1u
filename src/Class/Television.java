package Class;

public class Television implements Commandable 
{
	
	private int son = 0;

	public void activer() 
	{
		this.son += 10;
		
		if (this.son > 100)
			this.son = 100;
	}

	public void desactiver() 
	{
		this.son = 0;
	}

	public String toString() 
	{
		String r = "";
		r += "TÚlÚvision:" + son;
		return (r);
	}
}
