package Class;


public class LumiereCommandable implements Commandable{
	
	private Lumiere Lum;
	
	public LumiereCommandable(Lumiere lumPara)
	{
		this.Lum = lumPara;
	}
	
	public void activer()
	{
		Lum.changerIntensite(1);
	}
	
	public void desactiver()
	{
		Lum.changerIntensite(0);
	}
	
	
	
}
