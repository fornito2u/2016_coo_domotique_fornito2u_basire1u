package Class;

public class Telecommande 
{
	private Commandable objetCom[];
	public static final int MAXTAILLE=1000;
	private int nbObjet;
	
	public Telecommande()
	{
		this.objetCom = new Commandable[MAXTAILLE];
		this.nbObjet = 0;
	}
	
	public void ajouterObjet(Commandable l)
	{
		this.objetCom[this.nbObjet] = l;
		this.nbObjet += 1;
	}
	
	public void activerObjet(int indiceObjet)
	{
		if(indiceObjet >= 0 && indiceObjet <= this.nbObjet)
		{
			this.objetCom[indiceObjet].activer();
		}
		else
		{
			System.out.println("L'indice d'objet incorrect");
		}
	}
	
	public void desactiverObjet(int indiceObjet)
	{
		if(indiceObjet >= 0 && indiceObjet <= this.nbObjet)
		{
			this.objetCom[indiceObjet].desactiver();
		}
		else
		{
			System.out.println("L'indice de la lampe est incorrect");
		}
	}
	
	public void activerTout()
	{
		for(int i = 0; i < this.nbObjet; i++)
		{
			this.objetCom[i].activer();
		}
	}
	
	public String toString()
	{
		String res1 =("Les lampes g�r�es par la t�l�commande sont : "+this.objetCom);
		String res2 =("Les chaines hifi g�r�es par la t�l�commande sont : "+this.objetCom);
		return(res1+"\n"+res2);
	}

	public Commandable[] getObjet() 
	{
		return this.objetCom;
	}
	
	public int getNbObjet()
	{
		return this.nbObjet;
	}
}
