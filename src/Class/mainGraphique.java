package Class;


public class mainGraphique {

	public static void main(String args[])
	{
		Telecommande t=new Telecommande();
		
		
		Lampe l1=new Lampe("Lampe1");
		t.ajouterObjet(l1);
		
		Hifi h1 = new Hifi();
		t.ajouterObjet(h1);
		
		Lumiere lum1 = new Lumiere();
		LumiereCommandable adap1 = new LumiereCommandable(lum1);
		t.ajouterObjet(adap1);
		
		TelecommandeGraphique tg=new TelecommandeGraphique(t);
	}
	
}