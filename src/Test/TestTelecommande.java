package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Class.Lampe;
import Class.Lumiere;
import Class.LumiereCommandable;
import Class.Telecommande;

public class TestTelecommande {
	
	
	@Test
	/**
	 * Test si une premi�re lampe s'ajoute bien � la telecommande
	 */
	public void testAjouterLampeNumeroTelecommandeVide() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");
		
		// Test
		tele.ajouterObjet(aladin);
		
		// Verification
		assertEquals("La lampe devrait �tre cr�e a l'emplacement 0",aladin , tele.getObjet()[0]); 
	}
	
	@Test
	public void testAjouterLampeTelecommandeNonVide() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");
		Lampe aHuile = new Lampe("AHuile");
		
		// Test
		tele.ajouterObjet(aladin);
		tele.ajouterObjet(aHuile); 
		 
		// Verification
		assertEquals("La seconde lampe devrait �tre cr�e a l'emplacement 1",aHuile , tele.getObjet()[1]); 
	}
	
	@Test
	public void testGetLampe() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");
		Lampe aHuile = new Lampe("AHuile");
		
		// Test
		tele.ajouterObjet(aladin);
		tele.ajouterObjet(aHuile);
		
		// Verification
		assertEquals("Il ne devrait avoir ni lampe ni message d'erreur",null , tele.getObjet()[5]); 
	}
	
	@Test
	public void testAcitverLampePosition0() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");

		// Test
		tele.ajouterObjet(aladin);
		tele.activerObjet(0);
		
		// Verification
		assertEquals("La Lampe devrait s'allumer",true , ((Lampe) tele.getObjet()[0]).isAllume());
	}
	
	@Test
	public void testDesactiverLampePosition0() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");

		// Test
		tele.ajouterObjet(aladin);
		tele.activerObjet(0);
		tele.desactiverObjet(0);
		
		// Verification
		assertEquals("La Lampe devrait s'eteindre",false , ((Lampe) tele.getObjet()[0]).isAllume());
	}
	
	@Test
	public void testActiverLampePosition1() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");
		Lampe aHuile = new Lampe("AHuile");
		
		// Test
		tele.ajouterObjet(aladin);
		tele.ajouterObjet(aHuile);
		tele.activerObjet(1);
		
		// Verification
		assertEquals("La Lampe en position 1 devrait s'allumer",true , ((Lampe) tele.getObjet()[1]).isAllume());
		assertEquals("La Lampe en position 0 devrait rester eteinte",false , ((Lampe) tele.getObjet()[0]).isAllume());
	}
	
	@Test
	public void testDesactiverLampePosition1() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");
		Lampe aHuile = new Lampe("AHuile");
		
		// Test
		tele.ajouterObjet(aladin);
		tele.ajouterObjet(aHuile);
		tele.activerObjet(1);
		tele.activerObjet(0);
		tele.desactiverObjet(1);
		
		// Verification
		assertEquals("La Lampe en position 1 devrait s'eteindre",false , ((Lampe) tele.getObjet()[1]).isAllume());
		assertEquals("La Lampe en position 0 devrait rester allumer",true , ((Lampe) tele.getObjet()[0]).isAllume());
	}
	
	@Test
	public void testActiverLampeInexistante() 
	{
		// Preparation des donn�e 
		Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");
		Lampe aHuile = new Lampe("AHuile");
		tele.ajouterObjet(aladin);
		tele.ajouterObjet(aHuile);
		
		// Test	
		tele.activerObjet(1);
		tele.activerObjet(42);
		tele.desactiverObjet(10);

		
		// Verification
		assertEquals("La Lampe en position 1 devrait rester allumer",true ,((Lampe) tele.getObjet()[1]).isAllume());
		assertEquals("La Lampe en position 0 devrait rester eteinte",false ,((Lampe) tele.getObjet()[0]).isAllume());
		assertEquals("La Lampe en position 42 devrait pas exister",null ,tele.getObjet()[42]);
		assertEquals("La Lampe en position 10 devrait pas exister",null ,tele.getObjet()[10]);
	}
	
	@Test
	public void testActiverToutLampes() 
	{
		// Preparation des donn�e 
	    Telecommande tele = new Telecommande();
		Lampe aladin = new Lampe("Aladin");
		Lampe aHuile = new Lampe("AHuile");
		tele.ajouterObjet(aladin);
		tele.ajouterObjet(aHuile);
				
		// Test
		tele.activerTout();
		
		// Verification
		assertEquals("La Lampe en position 1 devrait s'allumer",true ,((Lampe) tele.getObjet()[1]).isAllume());
		assertEquals("La Lampe en position 0 devrait  s'allumer",true ,((Lampe) tele.getObjet()[0]).isAllume());	
	}
	

	@Test
	public void testActiverLumiere() 
	{
		// Preparation des donn�e
		Lumiere lum = new Lumiere();
		LumiereCommandable lumC= new LumiereCommandable(lum);
	    Telecommande tele = new Telecommande();
		tele.ajouterObjet(lumC);
		
		// Test
		lumC.activer();
		
		// Verification
		assertEquals("La lumiere devrait �tre activ�e ",0 ,((Lumiere) tele.getObjet()[0]).getLumiere());
	}
	
	@Test
	public void testDesactiverLumiere() 
	{
		// Preparation des donn�e
		Lumiere lum = new Lumiere();
		LumiereCommandable lumC= new LumiereCommandable(lum);
	    Telecommande tele = new Telecommande();
		tele.ajouterObjet(lumC);
		
		// Test
		lumC.activer();
		lumC.desactiver();
		
		// Verification
		assertEquals("La lumiere devrait �tre d�sactiv�e ",0 ,((Lumiere) tele.getObjet()[0]).getLumiere());
	}
}
